// Use these links at the end
// https://eng1003.monash/libs/countries.js
// https://eng1003.monash/libs/services.js

"use strict";

let currentDate;
let airportData;
let index

function checkValue(){
    if(!document.getElementById("input1Choice").value){
        alert("Select your trip country")
    }
    else if(!document.getElementById("input2Choice").value){
        alert("Select your trip airport")
    }
    else if(!document.getElementById("input3Choice").value){
        alert("Select trip date first")
    }
    else if(!document.getElementById("input4Choice").value){
        alert("Select your trip time")
    }
    else if((document.getElementById("input4Choice").value <= currentDate.toString().substring(16,21)) && (document.getElementById("input3Choice").value<=document.getElementById("input3Choice").min)){
        // Show user that he selected invalid option
        document.getElementById("input4Choice").style.backgroundColor="red"
    }
    else{
        document.getElementById("input4Choice").style.backgroundColor=""
        saveTripData()
        enableMap()
    }
}

function saveTripData(){
    //get airport object index
    index = airportData.findIndex((element) => element.name === document.getElementById("input2Choice").value )
}

function getAirports(){
    if(document.getElementById("input1Choice").value){
        let QueryString = "https://eng1003.monash/OpenFlights/airports/?country="+document.getElementById("input1Choice").value+"&callback=getAirportNames"
        let script = document.createElement("script")
        script.src = QueryString
        document.body.appendChild(script)
    }
    else if((document.getElementById("input1Choice").value === "") && document.getElementById("input2Choice").value){
        document.getElementById("input2Choice").value = ""
    }
}

function getAirportNames(airportArray){
    let output = ""
    airportData = airportArray
    for(let i=0; i<airportArray.length; i++){
        output += '<option id="input2" value="' + airportArray[i].name + '"></option>'
    }
    document.getElementById("input2").innerHTML = output
}

function clearValue(){
    document.getElementById("input1Choice").value = ""
    document.getElementById("input2Choice").value = ""
    document.getElementById("input3Choice").value = ""
    document.getElementById("input4Choice").value = ""
    document.getElementById("input4Choice").style.backgroundColor=""
    
    let routePolygon = map.getLayer("circle")
    if(routePolygon !== undefined){
        map.removeLayer("circle");
        map.removeSource("points");
    }

    airportArray = [];
    featuresArray = [];

    map.remove()
}

window.onload = (event) => {
    let dataValues = "";

    for (let i = 0; i < countryData.length; i++) {
        dataValues += '<option id="input1" value="' + countryData[i] + '"></option>'
    }

    document.getElementById("input1").innerHTML = dataValues

    currentDate = new Date()

    if (currentDate.getDate() < 10) {
        document.getElementById("input3Choice").min = currentDate.toISOString().substring(0, 8) + "0" + currentDate.getDate()
    }
    else { 
        document.getElementById("input3Choice").min = currentDate.toISOString().substring(0, 8) + currentDate.getDate() 
    }
};

mapboxgl.accessToken = 'pk.eyJ1IjoiYWJkdWxyYWZheTExMiIsImEiOiJja29pamUzbjAwb3k1Mm5rMjE0ZGY3djhkIn0.7mQg4p6wqUZfU5H50f_AKA';

let marker;
let popup;
let markerArray = [];
let popupArray = [];
let featuresArray = [];

let currentLocation;
let currentName;

let airportArray = [];
let tripArray = [];
let nameArray = [];
let routesArray = [];

let map;

const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIP_DATA_KEY = "tripLocalData";

function enableMap() {
    airportArray = airportData;
    console.log("this is airport array")
    console.log(airportArray)

    tripArray = [[airportArray[index].longitude, airportArray[index].latitude]];
    nameArray.push(airportArray[index].name)
    //idArray.push(airportArray[index].airportId)

    map = new mapboxgl.Map({
        container: "mapElement", // container id
        style: 'mapbox://styles/mapbox/streets-v10', // style URL
        center: tripArray[0],
        zoom: 9 // starting zoom
    });

    // Create a popup, but don't add it to the map yet.
    popup = new mapboxgl.Popup({
    closeButton: false,
    closeOnClick: false
    });

    marker = new mapboxgl.Marker().setLngLat(tripArray[0]).addTo(map)
    markerArray.push(marker)

    getCoordinateFeatures()
}

function getCoordinateFeatures() {
    for (let i = 0; i < airportArray.length; i++) {
        featuresArray.push({
            'type': 'Feature',
            'properties': {
                'description':'<p>' + airportArray[i].name + '</p>'
            },
            'geometry': {
                'type': 'Point',
                'coordinates': [airportArray[i].longitude, airportArray[i].latitude]
            }
        })
    }
    loadMap()
}

function loadMap() {
    map.on('load', function () {

        // Add a GeoJSON source with points.
        map.addSource('points', {
            'type': 'geojson',
            'data': {
                'type': 'FeatureCollection',
                'features': featuresArray
            }
        });

        // Add a circle layer
        map.addLayer({
            'id': 'circle',
            'type': 'circle',
            'source': 'points',
            'paint': {
                'circle-color': '#4264fb',
                'circle-radius': 8,
                'circle-stroke-width': 2,
                'circle-stroke-color': '#ffffff'
            }
        });

        // Center the map on the coordinates of any clicked circle from the 'circle' layer.
        map.on('click', 'circle', function (e) {
            currentLocation = e.features[0].geometry.coordinates
            map.flyTo({
                center: currentLocation
            });
            currentName = e.features[0].properties.description.slice(3, -4)
        });

        // Change the cursor to a pointer when the it enters a feature in the 'circle' layer.
        
        map.on('mouseenter', 'circle', function (e) {
            
            // Change the cursor style as a UI indicator.
            
            map.getCanvas().style.cursor = 'pointer';

            let coordinates = e.features[0].geometry.coordinates.slice();
            let description = e.features[0].properties.description;
            
            // Ensure that if the map is zoomed out such that multiple
            // copies of the feature are visible, the popup appears
            // over the copy being pointed to.
            
            while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
                coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
            }

            // Populate the popup and set its coordinates
            // based on the feature found.
            
            popup.setLngLat(coordinates).setHTML(description).addTo(map);
        });

        // Change it back to a pointer when it leaves.

        map.on('mouseleave', 'circle', function () {
            map.getCanvas().style.cursor = '';
            popup.remove();
        });
    });

    let loadSource = ()=>{
        if (map.isStyleLoaded()) {
            drawRoutes(nameArray,airportArray)
          map.off('data', loadSource);
        }
      }
      
      map.on('data', loadSource);
    
    
}

function addAirport(){
    tripArray.push(currentLocation)
    marker = new mapboxgl.Marker().setLngLat(currentLocation).addTo(map)
    markerArray.push(marker)
    nameArray.push(currentName)

    drawPolygon('route',tripArray,'#ff0000')
    
    let loadSource = ()=>{
        if (map.isStyleLoaded()) {
            drawRoutes(nameArray,airportArray)
          map.off('data', loadSource);
        }
      }
      
      map.on('data', loadSource);
}

function removeAirport(){
    tripArray.pop(currentLocation)
    let index = tripArray.length-1

    marker = markerArray.pop()
    marker.remove() 
    
    nameArray.pop()
    
    map.flyTo({center: tripArray[index]})

    drawPolygon('route',tripArray,'#ff0000')

    let loadSource = ()=>{
        if (map.isStyleLoaded()) {
            drawRoutes(nameArray,airportArray)
          map.off('data', loadSource);
        }
      }
      
      map.on('data', loadSource);
}

function drawPolygon(id,tripArray,color){
    
    let routePolygon = map.getLayer(id)
    if(routePolygon !== undefined){
        map.removeLayer(id);
        map.removeSource(id);
    }

    map.addLayer({
        id: id,
        type: 'line',
        source: {
            type: 'geojson',
            data: {
                "type": "FeatureCollection",
                "features": [
                  {
                    "type": "Feature",
                    "properties": {},
                    "geometry": {
                      "type": "LineString",
                      "coordinates": tripArray
                    }
                  }
                ]
              }
        },
        layout: {},
        paint: {
            'line-color': color,
            'line-opacity': 1
        }
    });
}
let idArray = [];
function drawRoutes(nameArray,airportArray){

    for(let i=0; i<nameArray.length; i++){
        for(let j=0; j<airportArray.length;j++){
            if(nameArray[i] === airportArray[j].name){
                idArray.push(airportArray[j])
            }
        }
    }
    
    let index = idArray.length-1

    let QueryString = "https://eng1003.monash/OpenFlights/routes/?sourceAirport="+idArray[index].airportId+"&callback=routes"
    let script = document.createElement("script")
    script.src = QueryString
    document.body.appendChild(script)

    
}
let uniqueRoutes = []
function routes(data){
    console.log("this is routes")
    console.log(data)

    for(let i=0; i<data.length;i++){
        for(let j=0; j<airportArray.length; j++){
            if(data[i].destinationAirportId.toString() === airportArray[j].airportId){
                routesArray.push([airportArray[j].longitude, airportArray[j].latitude])
            }
        }
    }

    let leftArray = []
    let rightArray = []

    for(let i=0;i<routesArray.length;i++){
        leftArray.push(routesArray[i][0])
        rightArray.push(routesArray[i][1])
    }
   
    let uniqueleftRoutes = [ ...new Set(leftArray) ];

    console.log("this is uniqueleftRoutes")
    console.log(uniqueleftRoutes)
    
    let uniquerightRoutes = [ ...new Set(rightArray) ];
    console.log("this is uniqueRightRoutes")
    console.log(uniquerightRoutes)

    for(let i=0; i<uniquerightRoutes.length; i++){
        uniqueRoutes.push([uniqueleftRoutes[i],uniquerightRoutes[i]])
    }

    let index = tripArray.length-1
    for (let i = 0; i < countryData.length; i++) {
        let routePolygon = map.getLayer("routes"+i)
        if (routePolygon !== undefined) {
            map.removeLayer("routes"+i);
            map.removeSource("routes"+i);
        }
    }
    
    let allRoutes;
    
    for(let i=0; i<uniqueRoutes.length; i++){
        allRoutes = [tripArray[index]]
        allRoutes.push(uniqueRoutes[i])
        drawPolygon("routes"+i,allRoutes,'#73ad21')
        console.log("this is all routes")
        console.log(allRoutes)
        allRoutes = []
    }

    console.log("this is unique routes")
    console.log(uniqueRoutes)
    
}

function saveTrip(){
     // Confirm with the user
   if (confirm("Confirm save trip")) {
    // runs if user clicks 'OK'
    let newTrip = new trip(nameArray, tripArray, document.getElementById("input3Choice").value, document.getElementById("input4Choice").value)
    console.log(newTrip)
    
    updateTripList(newTrip)
    alert("Trip saved");
    window.location = "TripSummary.html";
  }
  else {
    // runs if user clicks 'Cancel'
    alert("Trip deleted");
    window.location = "HomePage.html";
  }
}

function updateTripList(newTrip){
    let tripList = new TripList()
    
    //get triplist information

    let triplistData = JSON.parse(localStorage.getItem(TRIP_DATA_KEY))
    if(triplistData){
        console.log("this is triplist Data")
        console.log(triplistData)
        tripList.fromData(triplistData)
    }

    //push trip data into it

    tripList.addTrip(newTrip)
    
    //save triplist in local storage

    localStorage.setItem(TRIP_DATA_KEY,JSON.stringify(tripList))
    
    //save trip index

    localStorage.setItem(TRIP_INDEX_KEY,JSON.stringify(tripList.count-1))
}