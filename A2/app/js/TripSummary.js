"use strict";

const TRIP_INDEX_KEY = "selectedTripIndex";
const TRIP_DATA_KEY = "tripLocalData";

mapboxgl.accessToken = 'pk.eyJ1IjoiYWJkdWxyYWZheTExMiIsImEiOiJja29pamUzbjAwb3k1Mm5rMjE0ZGY3djhkIn0.7mQg4p6wqUZfU5H50f_AKA';

let map;
let tripValue;

function getTripData(){
    let tripList = new TripList()
    let index = JSON.parse(localStorage.getItem(TRIP_INDEX_KEY))
    
    //get triplist information

    let triplistData = JSON.parse(localStorage.getItem(TRIP_DATA_KEY))
    tripList.fromData(triplistData)

    //get trip from triplist

    let newTrip = tripList.tripArray(index)
    
    console.log("this is trip data")
    console.log(newTrip)

    tripValue = newTrip

    document.getElementById("text").innerText += "This trip has " + tripValue.stops +" stops \n"
    document.getElementById("text").innerText += "This trip starts from " + tripValue.name(0) + " and ends at " + tripValue.name(tripValue.count-1) + "\n"
}

function getMap(){
    map = new mapboxgl.Map({
        container: "map", // container id
        style: 'mapbox://styles/mapbox/streets-v10', // style URL
        //center: tripValue.locations[0],
        zoom: 9 // starting zoom
    });
}

window.addEventListener('load', (event) => {
    console.log('page is fully loaded');
    getTripData()
    getMap()
  });