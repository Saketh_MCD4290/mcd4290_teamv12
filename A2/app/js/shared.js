"use strict";

class trip{
    constructor(nameArray, tripArray, tripDate, tripTime){
        this._nameArray = nameArray
        this._tripArray = tripArray
        this._tripDate = tripDate
        this._tripTime = tripTime
    }

    name(index){
        return this._nameArray[index]
    }

    get count(){
        return this._tripArray.length
    }

    get stops(){
        return this._tripArray.length-1
    }

    get locations(){
        return this._tripArray
    }

    get date(){
        return this._tripDate
    }
    
    get time(){
        return this._tripTime
    }

    fromData(tripObject){
        let nameArray = []
        for(let i=0; i<tripObject._nameArray.length;i++){
            nameArray.push(tripObject._nameArray[i])
        }
        this._nameArray = nameArray
        
        let tripArray = []

        for(let i=0; i<tripObject._tripArray.length;i++){
            tripArray.push(tripObject._tripArray[i])
        }
        this._tripArray = tripArray

        this._tripDate = tripObject._tripDate
        this._tripTime = tripObject._tripTime
    }

}

class TripList{
    constructor(){
        this._trips = [];
    }

    get trips(){
        return this._trips
    }

    get count(){
        return this._trips.length
    }

    tripArray(index){
        return this._trips[index]
    }

    addTrip(trip){
        this._trips.push(trip)
    }

    removeTrip(trip){
        for(let i=0; i<this.count; i++){
            if((this._trips.date === trip.date)){
                this._trips.splice(i,1)
            }
        }
    }

    fromData(tripListObject){
        for(let i = 0; i < tripListObject._trips.length; i++) {
            let newTrip = new trip();
            newTrip.fromData(tripListObject._trips[i]);
            this._trips.push(newTrip);
        }
    }
}